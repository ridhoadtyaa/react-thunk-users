import { useDispatch, useSelector } from 'react-redux';
import { getUsers } from './feature/usersSlice';
import { Toaster } from 'react-hot-toast';
import Loading from './components/Loading';

function App() {
	const dispatch = useDispatch();
	const { entities, loading } = useSelector(state => state.users);

	const getUserHandler = () => {
		dispatch(getUsers());
	};

	return (
		<>
			<Toaster />
			<header className="w-11/12 max-w-2xl mx-auto py-8">
				<h1 className="text-2xl font-bold">Users</h1>
			</header>

			<main className="w-11/12 max-w-2xl mx-auto">
				<button className="bg-sky-500 rounded-md py-1 px-3 text-white" onClick={getUserHandler}>
					Get User
				</button>
				{loading && <Loading />}
				<div className="mt-4 grid grid-cols-2 gap-4">
					{entities.length > 0 &&
						entities?.map(user => (
							<div className="rounded-md border border-slate-300 p-3" key={user.id}>
								<p>{user.name}</p>
								<p>{user.username}</p>
								<p>{user.email}</p>
								<p>{user.phone}</p>
							</div>
						))}
				</div>
			</main>
		</>
	);
}

export default App;
