import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import toast from 'react-hot-toast';

const initialState = {
	entities: [],
	loading: false,
};

export const getUsers = createAsyncThunk(
	//action type string
	'users/getUsers',
	// callback function
	async () => {
		const res = await fetch('https://jsonplaceholder.typicode.com/users').then(data => data.json());
		return res;
	}
);

export const usersSlice = createSlice({
	name: 'users',
	initialState,
	reducers: {},
	extraReducers: {
		[getUsers.pending]: state => {
			state.loading = true;
		},
		[getUsers.fulfilled]: (state, { payload }) => {
			state.loading = false;
			state.entities = payload;
			toast.success('Get User successful');
		},
		[getUsers.rejected]: state => {
			state.loading = false;
			toast.error('Failed to get user');
			console.log('gagal');
		},
	},
});

// Action creators are generated for each case reducer function
// export const { increment } = usersSlice.actions;

export default usersSlice.reducer;
