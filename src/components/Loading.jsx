import { ImSpinner2 as Spinner } from 'react-icons/im';

import React, { Component } from 'react';

class Loading extends Component {
	render() {
		return (
			<div className="flex items-center justify-center">
				<Spinner className="h-24 w-24 animate-spin text-slate-600" />
			</div>
		);
	}
}

export default Loading;
